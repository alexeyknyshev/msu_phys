#-------------------------------------------------
#
# Project created by QtCreator 2015-05-10T10:52:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gas_model
TEMPLATE = app


SOURCES += main.cpp\
        mainwidget.cpp

HEADERS  += mainwidget.h \
    normaldistribution.h

FORMS    += mainwidget.ui

RESOURCES += \
    res.qrc
