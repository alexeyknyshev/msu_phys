#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QDateTime> /// current time for random seed

#include "normaldistribution.h"

#define RAD_TO_DEG 57.2957795
#define CYLINDER_RADIUS 50
#define CYLINDER_HEIGHT 100
#define PARTICLE_SIZE 1
#define GAS_CONST 8.3144621

#define ATMOSPHERE_MOLE_MASS 0.02898

// функция получения случайного вектора по алгоритму Sphere Point Picking
// http://mathworld.wolfram.com/SpherePointPicking.html
static QVector3D getRandomUnitVector()
{
	// получаем два псевдонормально распределённых числа из [-1;1]

	// ДОПУЩЕНИЕ! алгоритм Sphere Point Picking предполагает, что
	// для х1 и х2 выборка ведётся из независимых случайных величин.
	// В данном случае мы имеем достаточно хорошую выборку (10000 валичин)
	// для нашей модели, что можем принебречь этим условием

	qreal x1 = getRandomNormalDistrubutedNormalized();
	qreal x2 = getRandomNormalDistrubutedNormalized();

	qreal x = 2 * x1 * qSqrt(1 - x1 * x1 - x2 * x2);
	qreal y = 2 * x2 * qSqrt(1 - x1 * x1 - x2 * x2);
	qreal z = 1 - 2 * (x1 * x1 + x2 * x2);

	return QVector3D(x, y, z);
}

// распределение Максвелла в чистом виде не используется, т.к. оно
// может использоваться только для БОЛЬШОГО числа частиц,
// стандартными методами смоделировать систему, состаящую из > 10^6
// частиц не возможно

// M - относительная молекулярная масса
// T - температура в градусах Кельвина
static qreal getRandomSpeed(qreal M, qreal T)
{
	// наиболее вероятная скорость
	const qreal mostProbableSpeed = qSqrt(2 * GAS_CONST * T / M);
	return mostProbableSpeed;
}

static QColor getRandomColor()
{
	return QColor(qrand() % 256, qrand() % 256, qrand() % 256);
}

static QVector3D getMirrorVector(const QVector3D &vec, const QVector3D &normal)
{
	return vec - 2.0 * QVector3D::dotProduct(vec, normal) * normal;
}

MainWidget::MainWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MainWidget)
{
	// инициализируем генератор случайных чисел текущим временем с начала эпохи
	// https://en.wikipedia.org/wiki/Unix_time
	qsrand(QDateTime::currentMSecsSinceEpoch());

	ui->setupUi(this);

	QGraphicsScene *scene = new QGraphicsScene; // создаём сцену для отрисовки
	ui->graphicsView->setScene(scene); // устанавливаем сцену для отрисовки

	cylinderItem = new QGraphicsEllipseItem(-CYLINDER_RADIUS, -CYLINDER_RADIUS,
											2 * CYLINDER_RADIUS , 2 * CYLINDER_RADIUS);

	// создаём красную метку на поверхности цилиндра, чтобы наблюдать вращение
	QGraphicsEllipseItem *markItem = new QGraphicsEllipseItem(CYLINDER_RADIUS, 0, 2, 2, cylinderItem);

	// сделаем толщину стенок равной 1
	QPen pen;
	pen.setWidth(1);
	cylinderItem->setPen(pen);

	// метку нарисуем красным цветом
	pen.setColor(QColor(Qt::red));
	markItem->setPen(pen);

	ui->graphicsView->scene()->addItem(cylinderItem);
	ui->graphicsView->setSceneRect(   -(CYLINDER_RADIUS + 10),    -(CYLINDER_RADIUS + 10),
								   2 * (CYLINDER_RADIUS + 10), 2 * (CYLINDER_RADIUS + 10));

	// включаем сглаживание, чтобы не видеть рубцов
	ui->graphicsView->setRenderHint(QPainter::Antialiasing);
	ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);

	// создаём таймер для обновления сцены
	timerId = 0;

	// обновляем сцену раз в 33 мс
	updateInterval = 33;
	timerId = startTimer(updateInterval);

	// когда изменяем количество частиц в графическом интерфейсе, вызываем функцию particlesCountChanged(int)
	connect(ui->particlesCountBox, SIGNAL(valueChanged(int)), this, SLOT(particlesCountChanged(int)));
}

MainWidget::~MainWidget()
{
	// завершаем работу таймера
	killTimer(timerId);
	delete ui;
}

// функция, вызывающаяся по таймауту
void MainWidget::timerEvent(QTimerEvent *event)
{
	const float deltaTime = (float)updateInterval / 1000.0f;

	// поворачиваем цилиндр
	double deltaRotation = ui->rotationVelocityBox->value() * RAD_TO_DEG * deltaTime;
	cylinderItem->setRotation(cylinderItem->rotation() + deltaRotation);

	const double cylinderLinearVel = deltaRotation * CYLINDER_RADIUS;

	// перемещаем частицы
	for (int i = 0; i < particlesList.size(); ++i)
	{
		Particle &particle = particlesList[i];
		QVector3D deltaMovement = particle.velocity * deltaTime;
		particle.setPosition(particle.getPosition() + deltaMovement);

		qreal x = particle.getPosition().x();
		qreal y = particle.getPosition().y();
		qreal z = particle.getPosition().z();

		QVector3D radiusVec = particle.getPosition();
		const qreal sqrMagnitude = x * x + y * y;

		// отражение от цилиндрической стенки
		if (sqrMagnitude >= CYLINDER_RADIUS * CYLINDER_RADIUS)
		{
			const qreal magnitude = qSqrt(sqrMagnitude);

			x = x / magnitude * CYLINDER_RADIUS - 0.5;
			y = y / magnitude * CYLINDER_RADIUS - 0.5;

			radiusVec.setX(x);
			radiusVec.setY(y);

			particle.velocity = getMirrorVector(particle.velocity, radiusVec.normalized());
			const qreal velMag = particle.velocity.length();

			QVector3D res = QVector3D::crossProduct(radiusVec.normalized(), QVector3D(0, 0, 1.0f));
			particle.velocity -= res * cylinderLinearVel;
			particle.velocity.normalize();
			particle.velocity *= velMag;
		}

		// отражение от верхнего донышка
		if (z > CYLINDER_HEIGHT)
		{
			z = CYLINDER_HEIGHT;
			particle.velocity = getMirrorVector(particle.velocity, QVector3D(0, 0, 1.0f));
		}
		else if (z < 0.0f)
		{
			z = 0.0f;
			particle.velocity = getMirrorVector(particle.velocity, QVector3D(0, 0, -1.0f));

		}
		radiusVec.setZ(z);

		particle.setPosition(radiusVec);
		particle.item->setPos(radiusVec.x(), radiusVec.y());

		for (int i = 0; i < particlesList.size(); ++i)
		{
			Particle &particle1 = particlesList[i];
			for (int j = i + 1; j < particlesList.size(); ++j)
			{
				Particle &particle2 = particlesList[j];
				QVector3D deltaVector = particle1.getPosition() - particle2.getPosition();
				QVector3D deltaVectorNorm = deltaVector.normalized();

				// столкновение
				if (deltaVector.lengthSquared() < (2 * particle1.getRadius()) * (2 * particle1.getRadius()))
				{
					// грубо расчитываем новые скорости
					QVector3D vel1Norm = particle1.velocity.normalized();
					QVector3D vel2Norm = particle2.velocity.normalized();

					qreal vel1Magnitude = particle1.velocity.length();
					qreal vel2Magnitude = particle2.velocity.length();

					particle1.velocity = getMirrorVector(vel1Norm, deltaVectorNorm) * vel2Magnitude;
					particle2.velocity = getMirrorVector(vel2Norm, deltaVectorNorm) * vel1Magnitude;
				}
			}
		}
	}
}

// функция, вызывающаяся при изменении геометрии главного окна
void MainWidget::resizeEvent(QResizeEvent *event)
{
	// сделаем так, чтобы цилиндр занимал весь видимый нам участок сцены
	QRectF rect = cylinderItem->rect();
	rect.setWidth(rect.width() + 20);
	rect.setHeight(rect.height() + 20);

	ui->graphicsView->fitInView(rect, Qt::KeepAspectRatio);
}

// функция вызывается при показе главного окна (при старте программы)
void MainWidget::showEvent(QShowEvent *event)
{
	// сделаем так, чтобы цилиндр занимал весь видимый нам участок сцены
	QRectF rect = cylinderItem->rect();
	rect.setWidth(rect.width() + 20);
	rect.setHeight(rect.height() + 20);

	ui->graphicsView->fitInView(rect, Qt::KeepAspectRatio);
}

// функция вызывается при изменении значения в поле "количество частиц"
void MainWidget::particlesCountChanged(int count)
{
//	qDebug() << "paticles count: " << count;

	int particlesCountDelta = count - particlesList.size();
	if (particlesCountDelta > 0)
	{
		const qreal T = ui->temperatureBox->value();

		while (particlesCountDelta > 0)
		{
			Particle particle;

            particle.item = new QGraphicsPixmapItem(QPixmap(":/star.png"));
            particle.item->setScale(0.05);

			QPen pen;
			QBrush brush;

			QColor particleColor = getRandomColor();

			pen.setColor(particleColor); // цвет контура
			brush.setColor(particleColor); // цвет заливки
			brush.setStyle(Qt::SolidPattern); // полная заливка цветом

//			particle.item->setPen(pen);
//			particle.item->setBrush(brush);

			particle.velocity = getRandomUnitVector() * getRandomSpeed(ATMOSPHERE_MOLE_MASS, T);

			QVector3D pos = getRandomUnitVector() * (CYLINDER_RADIUS - 2);
			particle.setPosition(pos);

			ui->graphicsView->scene()->addItem(particle.item);

			particlesList.append(particle);

			particlesCountDelta--;
		}
	}
	else
	{
		int removeCount = qAbs(particlesCountDelta);
		while (removeCount > 0 && particlesList.size() > 0)
		{
			Particle particle = particlesList.last();
			particlesList.removeLast();
			ui->graphicsView->scene()->removeItem(particle.item);
			delete particle.item;
			removeCount--;
		}
	}
}
