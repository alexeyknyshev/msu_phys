#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QGraphicsItem>
#include <QVector3D>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
	Q_OBJECT

public:
	explicit MainWidget(QWidget *parent = 0);
	~MainWidget();

	void timerEvent(QTimerEvent *event);
	void resizeEvent(QResizeEvent *event);
	void showEvent(QShowEvent *event);

private slots:
	void particlesCountChanged(int count);

private:
	Ui::MainWidget *ui;

    QGraphicsEllipseItem *cylinderItem;

	struct Particle
	{
        QGraphicsPixmapItem *item;
		QVector3D velocity;

		void setPosition(const QVector3D &pos)
		{
			position = pos;
			item->setPos(pos.x(), pos.y());
		}

		QVector3D getPosition()
		{
			return position;
		}

		qreal getRadius()
		{
			return item->boundingRect().width() / 2;
		}

	private:
		QVector3D position;
	};

	QList<Particle> particlesList;


	int timerId;
	int updateInterval;
};

#endif // MAINWIDGET_H
